# Segment
[![pipeline status](https://gitlab.com/jfavlam/segment/badges/master/pipeline.svg)](https://gitlab.com/jfavlam/segment/commits/master)    

Easily generate bitmask for [fourteen-segment display](https://en.wikipedia.org/wiki/Fourteen-segment_display) (FSD).   

[ONLINE TOOL](https://jfavlam.gitlab.io/segment/)

## Context
I've created this little tool to visually generate bitmask for my [Four Letter pHAT](https://shop.pimoroni.com/products/four-letter-phat).   
Simply draw your specific characters or symbols, copy generated mask and include it in your Python script :    
```python
my_char = 0b00100101000000 # Example for 'S' character
fourletterphat.set_digit_raw(0, my_char) # On first segment
```    
See more in Four Letter pHAT's [documentation](http://docs.pimoroni.com/fourletterphat/#fourletterphat.fourletterphat.set_digit_raw).

## Getting Started

`npm install` to install dependencies    
`npm run serve` to run dev server

## Built With

- [Vue.js](https://vuejs.org/) - JavaScript framework

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.

See [LICENSE](https://gitlab.com/jfavlam/segment/blob/master/LICENSE) file.
