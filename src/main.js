import Vue from 'vue';
import App from './App.vue';
import VueClipboard from 'vue-clipboard2';
import Notifications from 'vue-notification';
import $ from 'jquery';
window.$ = $;

Vue.use(VueClipboard);
Vue.use(Notifications);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount('#app');
