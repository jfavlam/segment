// vue.config.js
module.exports = {
  lintOnSave: false,
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    }
  },
  css: {
    loaderOptions: {
      sass: {
        // @/ is an alias to src/
        data: `@import "@/styles/style.scss";`
      }
    }
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/segment/'
    : '/'
};
